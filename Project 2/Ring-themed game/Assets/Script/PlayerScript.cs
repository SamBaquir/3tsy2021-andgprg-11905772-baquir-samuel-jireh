using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{

    public GameObject GameManager;
    private Rigidbody boxRB;
    private float speed = 7f, jumph = 3f, jumpforce = 2f;
    private Vector3 jump, inputVector;
    [SerializeField]
    public bool playerAlive;
    public AudioSource source;
    public AudioClip jumpSound;

    void Start()
    {
        jump = new Vector3(0f, jumph, 0f);
        boxRB = transform.GetChild(0).GetComponent<Rigidbody>();
    }

    public void Update()
    {
        if(GetComponent<HeartScript>().life > 0)
        {
            playerAlive = true;
            playing();
            if (GameManager.GetComponent<GameManager>().score >= 5 && GameManager.GetComponent<GameManager>().score < 50) jumpBox();
            moveBox();
        }
        else if (GetComponent<HeartScript>().life <= 0)
        {
            playerAlive = false;
            Destroy(this.gameObject, 5);
        }
    }

    private void moveBox()
    {
        boxRB.velocity = inputVector;
    }

    public void playing()
    { 
        if(GetComponent<HeartScript>().life > 0)
        {
            inputVector = new Vector3(Input.GetAxis("Horizontal") * speed, boxRB.velocity.y, Input.GetAxis("Vertical") * speed);
        }
    }

    public void jumpBox()
    {
        if (boxRB.velocity.y <= 0 && transform.GetChild(0).GetComponent<CheckCollision>().collideWithFloor)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                source.PlayOneShot(jumpSound);
                boxRB.AddForce(jump * jumpforce, ForceMode.Impulse);
                transform.GetChild(0).GetComponent<CheckCollision>().collideWithFloor = false;
                GameManager.GetComponent<GameManager>().score-=5;
            }
        }
        
    }
}