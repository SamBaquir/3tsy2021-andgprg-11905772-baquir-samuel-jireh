using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    [SerializeField]
    private int speed = 0;
    public GameObject ObstaclePreFab, Player, GameManager;
    private float delayTimer = 0;
    private bool delayOver = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        playerSpawned();
        spawnerLogic();
    }

    public void playerSpawned()
    {
        if (Player.GetComponent<HeartScript>().life <= 0 || GameManager.GetComponent<GameManager>().score >= 50)
        {
            Destroy(this.gameObject);
        }
    }

    private void spawnerLogic()
    {
        if (!delayOver)
        {
            delayTimer++;
        }
        if (delayTimer == 120)
        {
            Spawn();
            delayOver = true;
            delayTimer = 121;
        }
        if (transform.position.x <= -5)
        {
            speed = -speed;
        }
        if (transform.position.x >= 5)
        {
            speed = -speed;
        }
        this.transform.Translate(speed * Time.deltaTime, 0, 0);
    }


    private void Spawn()
    { 
        StartCoroutine("SpawnObstacle");
    }

    private IEnumerator SpawnObstacle()
    {
        Destroy(Instantiate(ObstaclePreFab, transform.position, transform.rotation), 3.0f);

        yield return new WaitForSeconds(Random.Range(.4f, .6f));
        StartCoroutine("SpawnObstacle");
    }

}
