using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckCollision : MonoBehaviour
{
    public AudioClip Damage, deathByObstacle, deathByFalling;
    public AudioSource audioSource;
    public GameObject spawn, player;
    public bool collideWithFloor;
    Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        outOfPlatform();
    }

    private void OnCollisionEnter(Collision other)
    {
        //Checks if floor collides
        if (other.gameObject.tag == "Floor")
        {
            collideWithFloor = true;
        }

        //Checks if obstacle collides
        if (other.gameObject.tag == "Obstacle")
        {
            if(player.GetComponent<HeartScript>().life > 1)
            {
                audioSource.PlayOneShot(Damage);
                player.GetComponent<HeartScript>().TakeDamage(1);
                collideWithFloor = false;
                rb.transform.position = spawn.transform.position;
            }           
            else if (player.GetComponent<HeartScript>().life == 1)
            {
                audioSource.PlayOneShot(deathByObstacle);
                player.GetComponent<HeartScript>().TakeDamage(1);
                transform.GetChild(0).parent = null;
                ragDoll();
                Destroy(this.gameObject, 5);
            }
        }
    }

    private void outOfPlatform()
    {
        if (transform.position.y <= -2 && player.GetComponent<HeartScript>().life > 1)
        {
            audioSource.PlayOneShot(Damage);
            player.GetComponent<HeartScript>().TakeDamage(1);
            rb.transform.position = spawn.transform.position;
        }
        else if (transform.position.y <= -2 && player.GetComponent<HeartScript>().life == 1)
        {
            audioSource.PlayOneShot(deathByFalling);
            player.GetComponent<HeartScript>().TakeDamage(1);
            transform.GetChild(0).parent = null;
            Destroy(this.gameObject, 5);
        }
    }

    private void ragDoll()
    {
        rb.constraints = RigidbodyConstraints.None;
        rb.gameObject.GetComponent<Rigidbody>().AddForce(Vector3.up * 500.0f);
        rb.gameObject.GetComponent<Rigidbody>().AddForce(Vector3.back * 500.0f);
    }

    
}
