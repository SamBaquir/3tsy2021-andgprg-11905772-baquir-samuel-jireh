using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject Player;
    public float score = 0;
    public TextMeshProUGUI scoreCountUI;
    private float restartTimer = 0, nextLoadDelay = 0;
    public float scoreMultiplier;
    public bool gameWin = false;

    // Start is called before the first frame update
    void Start()
    {
        displayScore();
        GameObject.FindGameObjectWithTag("Music").GetComponent<MusicScript>().PlayMusic();
    }

    // Update is called once per frame
    void Update()
    {
        //If player wants to exit game
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        //Displays UI and Score
        displayScore();

        //Restarts game if player loses
        playerLose();

        //Loads next scene if player wins
        playerWin();
    }

    void playerWin()
    {
        if(score == 50)
        {
            nextLoadDelay++;
            if (nextLoadDelay >= 1000)
            {
                restartTimer++;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
        }
    }
    
    void playerLose()
    {
        if (Player.GetComponent<HeartScript>().life < 1)
        {
            restartTimer++;
            if (restartTimer >=1000)
            {
                restartTimer = 0;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }

    void displayScore()
    {
        scoreCountUI.text = "Score: " + score;
    }

}
