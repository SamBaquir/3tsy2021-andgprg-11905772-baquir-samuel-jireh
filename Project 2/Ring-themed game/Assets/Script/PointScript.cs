using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointScript : MonoBehaviour
{
    public GameObject GameManager, Player;
    public AudioClip scoreSound;
    public AudioSource audioSource;

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Obstacle")
        {
            if(Player.GetComponent<HeartScript>().life > 0 && GameManager.GetComponent<GameManager>().score < 50)
            {
                audioSource.PlayOneShot(scoreSound);
                GameManager.GetComponent<GameManager>().score += 1 * GameManager.GetComponent<GameManager>().scoreMultiplier;
            }
        }
    }
}
