using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveFloor : MonoBehaviour
{

    public int speed = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x <= -5)
        {
            speed = -speed;
        }
        if (transform.position.x >= 5)
        {
            speed = -speed;
        }
        this.transform.Translate(speed * Time.deltaTime, 0, 0);
    }
}
