using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    public float speed = 500;
    public Rigidbody rb;
    private float activeForwardSpeed;

    // Start is called before the first frame update
    void Start()
    {
      
        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward * speed;

    }

}
