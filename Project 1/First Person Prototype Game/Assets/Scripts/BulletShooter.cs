using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShooter : MonoBehaviour
{
    public Transform nozzle;
    public AudioClip pistolShot;
    public GameObject bulletPrefab;
    public AudioSource audioSource;
    public GameObject target;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            audioSource.PlayOneShot(pistolShot);
            Destroy(Instantiate(bulletPrefab, nozzle.transform.position, transform.rotation), 5.0f);
        }
    }
}
