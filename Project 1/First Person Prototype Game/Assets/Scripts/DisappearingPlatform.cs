using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisappearingPlatform : MonoBehaviour
{
    public Rigidbody rb;

    public Transform spawner;
    public Transform spawner1;
    public Transform spawner2;
    public Transform spawner3;

    public GameObject platformPrefab;

    public bool isSpawned = false;

    // Start is called before the first frame update
    void Start()
    {

        rb = GetComponent<Rigidbody>();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("player") && isSpawned == false)
        {
            Instantiate(platformPrefab, spawner.transform.position, transform.rotation);
            Instantiate(platformPrefab, spawner1.transform.position, transform.rotation);
            Instantiate(platformPrefab, spawner2.transform.position, transform.rotation);
            Instantiate(platformPrefab, spawner3.transform.position, transform.rotation);
            isSpawned = true;
            Destroy(this.gameObject);
        }
    }


}
