using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
    private float moveSpeed = 7.5f, strafeSpeed = 5.0f, rotateSpeed = 200.0f, activeMoveSpeed, activeStrafeSpeed;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float rotation = Input.GetAxis("LookKeys") * rotateSpeed;
        rotation *= Time.deltaTime;
        transform.Rotate(0, rotation, 0);

        activeMoveSpeed = Input.GetAxisRaw("Vertical") * moveSpeed;
        activeStrafeSpeed = Input.GetAxisRaw("Horizontal") * strafeSpeed;
        if (Input.GetKey(KeyCode.LeftShift) && moveSpeed < 15f && strafeSpeed < 15f)
        {
            moveSpeed *= 1.5f;
            strafeSpeed *= 1.5f;
        }
        else 
        {
            moveSpeed = 7.5f;
            strafeSpeed = 7.5f;
        }


        transform.position += transform.forward * activeMoveSpeed * Time.deltaTime;
        transform.position += transform.right * activeStrafeSpeed * Time.deltaTime;


    }
}
