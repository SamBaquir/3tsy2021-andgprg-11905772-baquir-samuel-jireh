using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondPlatformSpawner : MonoBehaviour
{
    public GameObject platformPreFab;


    void Start()
    {
        InvokeRepeating("Spawn", 3, 5);
    }

    void Spawn()
    {
        Destroy(Instantiate(platformPreFab, transform.position, transform.rotation), 4.0f);

    }

    // Update is called once per frame
    void Update()
    {



    }
}
