using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    
    public GameObject target;
    public bool timerIsRunning = false;
    public float timeRemaining = 5;

    // Update is called once per frame
    void Update()
    {
        GameObject.FindGameObjectWithTag("Music").GetComponent<MusicScript>().PlayMusic();
        

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
