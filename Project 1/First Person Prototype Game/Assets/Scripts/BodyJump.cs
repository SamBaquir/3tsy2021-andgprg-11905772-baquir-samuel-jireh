using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BodyJump : MonoBehaviour
{
    public float jumph = 4, jumpforce = 2;
    private Vector3 jump;
    private Rigidbody rb;
    private bool isGrounded;
    public AudioClip deathSound;
    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        jump = new Vector3(0f, jumph, 0f);
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (rb.velocity.y <= 0 && isGrounded)
        {
            if (Input.GetKeyDown("f"))
            {
                rb.AddForce(jump * jumpforce, ForceMode.Impulse);
                isGrounded = false;
            }
            
        }

        if(transform.position.y < -120 && transform.position.y > -121)
        {
            audioSource.PlayOneShot(deathSound);
        }


        if (transform.position.y < -200)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
    
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "floor")
        {
            rb.constraints = ~RigidbodyConstraints.FreezePositionY;
            isGrounded = true;
        }

    }
    
}
