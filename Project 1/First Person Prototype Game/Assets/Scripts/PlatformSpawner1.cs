using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpawner1 : MonoBehaviour
{
    public GameObject platformPreFab;


    void Start()
    {
        InvokeRepeating("Spawn", 1, 5);
    }

    void Spawn()
    {
        Destroy(Instantiate(platformPreFab, transform.position, transform.rotation), 4.0f);

    }

    // Update is called once per frame
    void Update()
    {
 

        
    }
}
