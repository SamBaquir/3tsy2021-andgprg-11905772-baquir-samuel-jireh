using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpawner2 : MonoBehaviour
{
    public GameObject platformPreFab;


    void Start()
    {
        InvokeRepeating("Spawn", 3, 6);
    }

    void Spawn()
    {
        Destroy(Instantiate(platformPreFab, transform.position, transform.rotation), 3.0f);

    }

    // Update is called once per frame
    void Update()
    {



    }
}
