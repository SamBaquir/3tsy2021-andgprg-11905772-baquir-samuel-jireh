using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TargetBehavior : MonoBehaviour
{
    public float hp = 3;
    public GameObject soundTriggerPreFab;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Bullet"))
        {
            hp--;

            if(hp <= 0)
            {
                Destroy(Instantiate(soundTriggerPreFab, transform.position, transform.rotation), 10.0f);
                Destroy(this.gameObject);
                //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }

        }
    }
}
