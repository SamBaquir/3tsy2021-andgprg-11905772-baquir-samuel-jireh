using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialPlatform : MonoBehaviour
{
    public Rigidbody rb;

    public Transform spawner;

    public GameObject targetPrefab, player;

    public bool isSpawned = false;

    // Start is called before the first frame update
    void Start()
    {

        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("player") && isSpawned == false)
        {
            Instantiate(targetPrefab, spawner.transform.position, transform.rotation);
            isSpawned = true;
        }
    }
}
