﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour
{
    Ray ray;
    RaycastHit hit;
    Vector3 originalPos;
    Quaternion originalRot;
    Vector3 towerPos;

    public GameObject gameManager;

    public GameObject currentTower;
    public float currentTowerCost = 0;

    public GameObject arrowPrefabTower;
    public GameObject cannonPrefabTower;
    public GameObject icePrefabTower;
    public GameObject firePrefabTower;

    //While building
    public GameObject box;

    //arrow
    //cannon
    //fire
    //ice

    // Start is called before the first frame update
    void Start()
    {
        originalPos = currentTower.transform.position;
        originalRot = currentTower.transform.rotation;
    }

    float minYAxis = 2;
    float maxYAxis = 2.1f;
    float minYBuildArea = 2.2f;
    float minXBuildArea = 5f;
    float maxXBuildArea = 58f;
    float minZBuildArea = 5f;
    float maxZBuildArea = 59f;

    Vector3 SnapToGrid(Vector3 towerObjectPos)
    {
        return new Vector3(Mathf.Round(towerObjectPos.x),
                            Mathf.Clamp(towerObjectPos.y, minYAxis, maxYAxis),
                            Mathf.Round(towerObjectPos.z));
    }

    void Update()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray, out hit))
        {
            Debug.Log(hit.point.x + hit.point.y);
            currentTower.transform.position = SnapToGrid(hit.point);
            Debug.DrawLine(ray.origin, hit.point, Color.green);

            Debug.Log("X is: " + hit.point.x + " Y is: " + hit.point.y + " Z is: " + hit.point.z);

            if (Input.GetMouseButtonDown(1))
            {
                currentTower.transform.Rotate(0.0f, -90.0f, 0.0f);
            }

            if ((hit.point.y < minYBuildArea || hit.point.x < minXBuildArea || hit.point.x > maxXBuildArea 
                || hit.point.z < minZBuildArea || hit.point.z > maxZBuildArea))
            {
                currentTower.GetComponent<Tower>().NonBuildable();
                return;
            }
            
            if (gameManager.GetComponent<GameManager>().goldCounter <= 0 || gameManager.GetComponent<GameManager>().goldCounter < currentTowerCost)
            {
                currentTower.GetComponent<Tower>().NonBuildable();
                return;
            }

            if(hit.collider.gameObject.tag == "Tower")
            {
                currentTower.GetComponent<Tower>().NonBuildable();
                return;
            }

            else
            {
                currentTower.GetComponent<Tower>().Buildable();
            }

            if(Input.GetMouseButtonDown(0) && gameManager.GetComponent<GameManager>().goldCounter >= currentTowerCost)
            {
                GameObject tempTower = (GameObject)Instantiate(currentTower, SnapToGrid(hit.point), currentTower.transform.rotation);
                gameManager.GetComponent<GameManager>().goldCounter -= currentTowerCost;
                tempTower.GetComponent<Tower>().Build();
                StartCoroutine(TimerToBuild(3, tempTower));
            }

        }
    }

    public void buildArrowTower()
    {
        currentTower = arrowPrefabTower;
        currentTowerCost = arrowPrefabTower.GetComponent<Tower>().gold;
        this.GetComponent<BuildManager>().enabled = true;
        this.GetComponent<SelectedTower>().enabled = false;
    }
    public void buildCannonTower()
    {
        currentTower = cannonPrefabTower;
        currentTowerCost = cannonPrefabTower.GetComponent<Tower>().gold;
        this.GetComponent<BuildManager>().enabled = true;
        this.GetComponent<SelectedTower>().enabled = false;
    }
    public void buildIceTower()
    {
        currentTower = icePrefabTower;
        currentTowerCost = icePrefabTower.GetComponent<Tower>().gold;
        this.GetComponent<BuildManager>().enabled = true;
        this.GetComponent<SelectedTower>().enabled = false;
    }
    public void buildFireTower()
    {
        currentTower = firePrefabTower;
        currentTowerCost = firePrefabTower.GetComponent<Tower>().gold;
        this.GetComponent<BuildManager>().enabled = true;
        this.GetComponent<SelectedTower>().enabled = false;
    }

    public void CancelBuild()
    {
        currentTower.transform.position = originalPos;
        currentTower.transform.rotation = originalRot;
        currentTowerCost = 0;
        this.GetComponent<BuildManager>().enabled = false;
        this.GetComponent<SelectedTower>().enabled = true;
    }

    IEnumerator TimerToBuild(float timer, GameObject temptower)
    {
        //add Debuff
        temptower.SetActive(false);
        Destroy(Instantiate(box, temptower.transform.position, Quaternion.identity), 3);
        float timeTick = 0;
        while (timer > timeTick)
        {
            timeTick += 1;
            //Debug.Log("Timer until tower built: " + timeTick);
            yield return new WaitForSeconds(1);
        }
        //remove the Debuff
        temptower.SetActive(true);
    }
}
        

