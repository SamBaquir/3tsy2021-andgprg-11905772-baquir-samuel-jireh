using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectedTower : MonoBehaviour
{
    Ray ray;
    RaycastHit hit;

    public Tower currentSelectedTwr;
    public Text towerInfoUI;

    public float upgradeDamageIncrement = 0;
    public float upgradeFireRateImprovement = 0;
    public float upgradeGoldIncrement = 0;

    public GameObject upgradeButton;
    public GameObject gameManager;

    void Start()
    {
        towerInfoUI.text = "Left Click to place, Right click to rotate object";
    }

    // Update is called once per frame
    void Update()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            if(Input.GetMouseButtonDown(0))
            {
                
                if (hit.collider.gameObject.GetComponent<Tower>() && hit.collider.gameObject.GetComponent<Tower>().upgrading == false)
                {

                    /*
                    Debug.Log(hit.collider.gameObject.name);
                    Debug.Log("Tower Cost: " + hit.collider.gameObject.GetComponent<Tower>().gold);
                    Debug.Log("Fire rate: " + hit.collider.gameObject.GetComponent<Tower>().fireRate);
                    Debug.Log("Damage: " + hit.collider.gameObject.GetComponent<Tower>().damage);
                    */
                    upgradeButton.SetActive(true);
                    currentSelectedTwr = hit.collider.gameObject.GetComponent<Tower>();
                    displayStats(currentSelectedTwr);
                }
            }
        }
    }


    public void displayStats(Tower selectedTower)
    {

        if (selectedTower.gameObject.GetComponent<Tower>().enabled)
        { 
            if (currentSelectedTwr.gameObject.GetComponent<Tower>().upgradeTimes < 5)
            {
                towerInfoUI.text = "Damage: " + selectedTower.gameObject.GetComponent<Tower>().damage + " + " + selectedTower.gameObject.GetComponent<Tower>().damageIncrement +
                                   "\nFire Rate: " + selectedTower.gameObject.GetComponent<Tower>().fireRate + " - " + selectedTower.gameObject.GetComponent<Tower>().fireRateImprovement +
                                    "\nTower Cost: " + selectedTower.gameObject.GetComponent<Tower>().gold + " + " + selectedTower.gameObject.GetComponent<Tower>().goldIncrement;
            }
        
            if (currentSelectedTwr.gameObject.GetComponent<Tower>().upgradeTimes >= 5)
            {
                towerInfoUI.text = "Tower is fully upgraded. Cannot be upgraded further.";
            }
        }
    }
    
    public void UpgradeTower()
    {
        if (currentSelectedTwr.gameObject.GetComponent<Tower>().upgradeTimes >= 5 || 
            gameManager.gameObject.GetComponent<GameManager>().goldCounter < currentSelectedTwr.gameObject.GetComponent<Tower>().gold) return;
        Debug.Log("Upgrade" + currentSelectedTwr.gameObject.name);
        towerInfoUI.text = "Left Click to place, Right click to rotate object";
        gameManager.GetComponent<GameManager>().goldCounter -= currentSelectedTwr.gameObject.GetComponent<Tower>().gold;
        StartCoroutine(UpgradeTower(3, currentSelectedTwr));
    }

    IEnumerator UpgradeTower(float timer, Tower selectedTower)
    {
        
        //add Debuff
        upgradeButton.SetActive(false);
        selectedTower.gameObject.GetComponent<Tower>().damage += selectedTower.gameObject.GetComponent<Tower>().damageIncrement;
        selectedTower.gameObject.GetComponent<Tower>().fireRate -= selectedTower.gameObject.GetComponent<Tower>().fireRateImprovement;
        selectedTower.gameObject.GetComponent<Tower>().gold += selectedTower.gameObject.GetComponent<Tower>().goldIncrement;
        selectedTower.gameObject.GetComponent<Tower>().upgrading = true;
        float timeTick = 0;
        while(timer > timeTick)
        {
            timeTick += 1;
            Debug.Log("Timer: " + timeTick);
            yield return new WaitForSeconds(1);
        }
        Debug.Log("Tower Upgraded");
        selectedTower.gameObject.GetComponent<Tower>().enabled = true;
        //remove the Debuff and upgrade the tower
        selectedTower.gameObject.GetComponent<Tower>().upgrading = false;
        selectedTower.gameObject.GetComponent<Tower>().damageIncrement += 2;
        selectedTower.gameObject.GetComponent<Tower>().goldIncrement += 50;
        selectedTower.gameObject.GetComponent<Tower>().upgradeTimes++;
    }
}
