using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public List<GameObject> enemies = new List<GameObject>();
    public GameObject[] spawners;
    public GameObject gameManager;
    public int spawnersEnabled = 0, goldIncrement = 0, healthIncrement = 0;
    public bool addedIncrement = false;

    // Update is called once per frame
    void Update()
    {
        if (enemies.Count >= 1 && !addedIncrement)
        {
            for (int i = 0; i < enemies.Count; i++)
            {
                enemies[i].GetComponent<EnemyBehaviour>().gold += goldIncrement;
                enemies[i].GetComponent<EnemyBehaviour>().health += healthIncrement;
            }
            addedIncrement = true;
        }

        if (enemies.Count == 0)
        {
            if (spawnersEnabled == 6)
            {
                for (int i = 0; i < spawners.Length; i++)
                {
                    spawners[i].GetComponent<SpawnerScript>().spawned = false;
                }

                addedIncrement = false;
                spawnersEnabled = 0;
                gameManager.GetComponent<GameManager>().waveCounter++;
                goldIncrement += 10;
                healthIncrement += 2;
            }

        }
    }

    IEnumerator StartNextWave(float timer)
    {
        float timeTick = 0;
        while (timer > timeTick)
        {
            timeTick += 1;
            Debug.Log("Next Wave starts in : " + timeTick);
            yield return new WaitForSeconds(1);
        }
       
    }
}
