using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawner : MonoBehaviour
{
    public GameObject gameManager;
    public GameObject bossPreFab;
    public Transform targetGoal;
    public bool spawned;
    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        if (!spawned && gameManager.GetComponent<GameManager>().waveCounter == 11)
        {
            Instantiate(bossPreFab, transform.position, transform.rotation);
            bossPreFab.GetComponent<EnemyBehaviour>().target = targetGoal;
            spawned = true;
        }
    }
}
