﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public MeshRenderer baseMeshRenderer;
    public SkinnedMeshRenderer skinnedHeadMeshRenderer;
    public Material originalMaterial;
    public Material redMaterial;
    public Material greenMaterial;
    public GameObject nozzle;
    public float gold = 0;
    public float damage = 0, fireRate = 0;
    public float upgradeTimes = 0;
    public bool upgrading = false;
    public float goldIncrement = 0, damageIncrement = 0, fireRateImprovement = 0;
    
    public void Awake()
    {
        nozzle.GetComponent<TowerAttack>().attackDamage = damage;
        nozzle.GetComponent<TowerAttack>().fireRate = fireRate;
    }

    public void Update()
    {
        nozzle.GetComponent<TowerAttack>().attackDamage = damage;
        nozzle.GetComponent<TowerAttack>().fireRate = fireRate;
    }

    public void Build()
    {
        baseMeshRenderer.material = originalMaterial;
        skinnedHeadMeshRenderer.material = originalMaterial;
        this.GetComponent<Collider>().enabled = true;
        nozzle.GetComponent<Collider>().enabled = true;
        nozzle.GetComponent<Collider>().isTrigger = true;
    }

    public void NonBuildable()
    {
        baseMeshRenderer.material = redMaterial;
        skinnedHeadMeshRenderer.material = redMaterial;
    }

    public void Buildable()
    {
        baseMeshRenderer.material = greenMaterial;
        skinnedHeadMeshRenderer.material = greenMaterial;
    }
}
