using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerScript : MonoBehaviour
{
    public GameObject gameManager;
    public GameObject manager;
    public GameObject goblinPreFab, drakePreFab;
    public Transform targetGoal;
    public bool spawned = false;

    // Update is called once per frame
    void Update()
    {
        if (!spawned && gameManager.GetComponent<GameManager>().waveCounter <= 5)
        {
            
            Instantiate(goblinPreFab, transform.position, transform.rotation);
            goblinPreFab.GetComponent<EnemyBehaviour>().spawnManager = manager;
            goblinPreFab.GetComponent<EnemyBehaviour>().target = targetGoal;
            manager.GetComponent<SpawnManager>().spawnersEnabled++;
            spawned = true;
            
        }
        else if (!spawned && (gameManager.GetComponent<GameManager>().waveCounter >= 6 && gameManager.GetComponent<GameManager>().waveCounter <= 10))
        {
            
            Instantiate(drakePreFab, transform.position, transform.rotation);
            drakePreFab.GetComponent<EnemyBehaviour>().spawnManager = manager;
            drakePreFab.GetComponent<EnemyBehaviour>().target = targetGoal;
            manager.GetComponent<SpawnManager>().spawnersEnabled++;
            spawned = true;
            
        }
    }

}
