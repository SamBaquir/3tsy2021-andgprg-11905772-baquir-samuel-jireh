using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerAttack : MonoBehaviour
{
    public float attackDamage = 0, fireRate = 0;
    public float slowDuration = 0, burnDuration = 0;
    public bool attackFlying, attackGround;
    public bool arrowType, cannonType, iceType, fireType;
    public bool enemyFound = false;
    public GameObject[] targetEnemy = new GameObject[1];

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.GetComponent<EnemyBehaviour>() && targetEnemy[0] == null) 
        {
            //Debug.Log("Enemy detected");
            targetEnemy[0] = collider.gameObject;   
        }
        if (collider.gameObject.GetComponent<EnemyBehaviour>() && collider.gameObject == targetEnemy[0])
        {
            if (arrowType && attackFlying && attackGround && targetEnemy[0] != null && (targetEnemy[0].tag == "FlyingEnemy" || targetEnemy[0].tag == "GroundEnemy"))
            {
                //Debug.Log("Tower Attacks");
                InvokeRepeating("arrowTowerFiringInvoked", 0.5f, fireRate);
            }
            else if (cannonType && attackGround && targetEnemy[0] != null && targetEnemy[0].tag == "GroundEnemy")
            {
                //Debug.Log("Cannon Attacks");
                InvokeRepeating("cannonTowerFiringInvoked", 0.5f, fireRate);
            }
            else if (iceType && attackFlying && attackGround && targetEnemy[0] != null && (targetEnemy[0].tag == "FlyingEnemy" || targetEnemy[0].tag == "GroundEnemy"))
            {
                //Debug.Log("Ice Tower Attacks");
                InvokeRepeating("iceTowerFiringInvoked", 0.5f, fireRate);
            }
            else if (fireType && attackFlying && attackGround && targetEnemy[0] != null && (targetEnemy[0].tag == "FlyingEnemy" || targetEnemy[0].tag == "GroundEnemy"))
            {
                //Debug.Log("Fire Tower Attack");
                InvokeRepeating("fireTowerFiringInvoked", 0.5f, fireRate);
            }
        }
    }

    void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.GetComponent<EnemyBehaviour>() && targetEnemy[0] == null)
        {
            targetEnemy[0] = collider.gameObject;
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject == targetEnemy[0])
        {
            targetEnemy[0] = null;
            CancelInvoke();
        }
    }

    void arrowTowerFiringInvoked()
    {
        if (targetEnemy[0] == null)
        {
            CancelInvoke();
            return;
        }
        targetEnemy[0].GetComponent<EnemyBehaviour>().takeDamage(attackDamage);   
    }

    void cannonTowerFiringInvoked()
    {
        if (targetEnemy[0] == null)
        {
            CancelInvoke();
            return;
        }
        targetEnemy[0].GetComponent<EnemyBehaviour>().takeSplashDamage(attackDamage);
    }
    void iceTowerFiringInvoked()
    {
        if (targetEnemy[0] == null)
        {
            CancelInvoke();
            return;
        }
        targetEnemy[0].GetComponent<EnemyBehaviour>().takeSlowingSplashDamage(attackDamage, slowDuration, targetEnemy[0]);
    }

    void fireTowerFiringInvoked()
    {
        if (targetEnemy[0] == null)
        {
            CancelInvoke();
            return;
        }
        targetEnemy[0].GetComponent<EnemyBehaviour>().takeBurningSplashDamage(attackDamage, burnDuration, targetEnemy[0]);
    }
}


/*
IEnumerator arrowTowerFiring(float firerate)
{
    float timeTick = 0;
    while (firerate > timeTick)
    {
        timeTick += 1;
        Debug.Log("Timer until tower attacks: " + timeTick);
        yield return new WaitForSeconds(1);
    }
    targetEnemy[0].GetComponent<EnemyBehaviour>().takeDamage(attackDamage);
}
*/




/*
//ground = "GroundEnemy";
if (collider.gameObject.GetComponent<EnemyBehaviour>())
{
    //targetEnemy = collider.gameObject.GetComponent<EnemyBehaviour>();
    Debug.Log(collider.gameObject.tag);
CompareTag("GroundEnemy") || collider.gameObject.CompareTag("FlyingEnemy")
}
*/

//(other.gameObject.tag == "GroundEnemy" || other.gameObject.tag == "FlyingEnemy") &&