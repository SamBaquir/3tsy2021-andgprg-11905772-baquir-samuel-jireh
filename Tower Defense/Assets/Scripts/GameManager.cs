using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public float waveCounter = 1;
    public Text waveCountUI;

    public float goldCounter = 100;
    public Text goldCountUI;

    // Start is called before the first frame update
    void Start()
    {
        waveCountUI.text = "Wave: " + waveCounter;
        goldCountUI.text = "Gold: " + goldCounter;
    }

    // Update is called once per frame
    void Update()
    {
        waveCountUI.text = "Wave: " + waveCounter;
        goldCountUI.text = "Gold: " + goldCounter;
        if (waveCounter > 11)
        {
            waveCountUI.text = "You win! Game will restart in 5 seconds";
            StartCoroutine(RestartGame(5));
        }    
    }
    IEnumerator RestartGame(float timer)
    {
        float timeTick = 0;
        while (timer > timeTick)
        {
            timeTick += 1;
            Debug.Log("Game restarts in: " + timeTick);
            yield return new WaitForSeconds(1);
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
