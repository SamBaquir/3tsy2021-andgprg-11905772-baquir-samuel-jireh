using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBehaviour : MonoBehaviour
{
    public float gold = 0, health = 0, speed = 0;
    NavMeshAgent agent;
    public GameObject gameManager;
    public GameObject spawnManager;
    public Transform target;
    public bool isBoss;
    
    
    private void Awake()
    {
        gameManager = GameObject.FindWithTag("GameManager");
        spawnManager = GameObject.FindWithTag("SpawnManager");
        target = GameObject.FindWithTag("Goal").transform;
        spawnManager.GetComponent<SpawnManager>().enemies.Add(this.gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        agent.SetDestination(target.position);    
    }

    // Update is called once per frame
    void Update()
    {
       if (health <= 0)
        {
            gameManager.GetComponent<GameManager>().goldCounter += gold;
            spawnManager.GetComponent<SpawnManager>().enemies.RemoveAt(0);
            Destroy(this.gameObject);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Goal")
        {
            spawnManager.GetComponent<SpawnManager>().enemies.RemoveAt(0);
            gold = 10;
            health = 10;
            Destroy(this.gameObject);
        }
    }

    public void takeDamage(float damage)
    {
        health -= damage;
    }

    public void takeSplashDamage(float damage)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 5f);
        foreach(Collider c in colliders)
        {
            if (c.GetComponent<EnemyBehaviour>())
            {
                c.GetComponent<EnemyBehaviour>().takeDamage(damage);
            }
        }
    }
    public void takeSlowingSplashDamage(float damage, float duration, GameObject targetEnemy)
    {
        StartCoroutine(ApplySelfSlowDebuff(duration, targetEnemy));
        Collider[] colliders = Physics.OverlapSphere(transform.position, 4f);
        foreach(Collider c in colliders)
        {
            if (c.GetComponent<EnemyBehaviour>())
            {
                c.GetComponent<EnemyBehaviour>().takeDamage(damage);
                c.GetComponent<EnemyBehaviour>().StartCoroutine(ApplySlowDebuffToOthers(duration, c));
            }
        }
    } 

    
    public void takeBurningSplashDamage(float damage, float burnduration, GameObject targetEnemy)
    {
        StartCoroutine(ApplySelfBurnDebuff(burnduration, targetEnemy));
        Collider[] colliders = Physics.OverlapSphere(transform.position, 4f);
        foreach (Collider c in colliders)
        {
            if (c.GetComponent<EnemyBehaviour>())
            {
                c.GetComponent<EnemyBehaviour>().takeDamage(damage);
                c.GetComponent<EnemyBehaviour>().StartCoroutine(ApplyBurnDebuffToOthers(burnduration, c));
            }
        }
    }
    
    
    IEnumerator ApplyBurnDebuffToOthers(float duration, Collider colliderpointer)
    {
        colliderpointer.GetComponent<EnemyBehaviour>().burningDebuff();
        //Apply Burn
        float timeTick = 0;
        while (duration > timeTick)
        {
            timeTick += 1;
            Debug.Log("Burning till: " + timeTick);
            yield return new WaitForSeconds(1);
        }
        colliderpointer.GetComponent<EnemyBehaviour>().cancelBurning();
        
    }

    IEnumerator ApplySelfBurnDebuff(float duration, GameObject self)
    {
        self.GetComponent<EnemyBehaviour>().burningDebuff();
        //Apply Burn
        float timeTick = 0;
        while (duration > timeTick)
        {
            timeTick += 1;
            yield return new WaitForSeconds(1);
        }
        self.GetComponent<EnemyBehaviour>().cancelBurning();
    }
    

    IEnumerator ApplySlowDebuffToOthers(float duration, Collider colliderpointer)
    {
        //Apply Slow debuff
        colliderpointer.GetComponent<NavMeshAgent>().speed = 1 ;
        float timeTick = 0;
        while (duration > timeTick)
        {
            timeTick += 1;
            yield return new WaitForSeconds(1);
        }
        //apply remove debuff
        colliderpointer.GetComponent<NavMeshAgent>().speed = speed;
    }

    IEnumerator ApplySelfSlowDebuff(float duration, GameObject self)
    {
        //Apply slow debuff
        self.GetComponent<NavMeshAgent>().speed = 1;
        float timeTick = 0;
        while (duration > timeTick)
        {
            timeTick += 1;
            yield return new WaitForSeconds(1);
        }
        //Remove slow debuff
        self.GetComponent<NavMeshAgent>().speed = speed;
    }

    void burningDebuff()
    {
        InvokeRepeating("burning", 1f, 1f);
    }

    void cancelBurning()
    {
        CancelInvoke();
    }

    void burning()
    {
        health -= 1;
    }
}
