using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GoalScript : MonoBehaviour
{
    public int maxHealth = 10;
    public int currentHealth;

    public GameObject spawnManager;
    public MeshRenderer meshRenderer;
    public Material redMaterial;
    public HealthBarScript healthBar;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
    }

    // Update is called once per frame
    void Update()
    {
        if (currentHealth <= 0)
        {
            spawnManager.SetActive(false);
            meshRenderer.material = redMaterial;
            currentHealth = 0;
            StartCoroutine(RestartGame(5));
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "GroundEnemy" || collision.gameObject.tag == "FlyingEnemy")
        {
            currentHealth--;
            healthBar.SetHealth(currentHealth);
        }
        if (collision.gameObject.GetComponent<EnemyBehaviour>().isBoss == true)
        {
            currentHealth -= 10;
            healthBar.SetHealth(currentHealth);
        }
    }
    IEnumerator RestartGame(float timer)
    {
        float timeTick = 0;
        while (timer > timeTick)
        {
            timeTick += 1;
            Debug.Log("Game restarts in: " + timeTick);
            yield return new WaitForSeconds(1);
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    
}
